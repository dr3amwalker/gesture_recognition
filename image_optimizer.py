#!/usr/bin/python
# -*- coding: UTF-8 -*-
class ImageOptimizer:

    # калибровка камеры
    @staticmethod
    def calibrate():
        n_boards=0  #no of boards
        board_w=int(sys.argv[1])    # number of horizontal corners
        board_h=int(sys.argv[2])    # number of vertical corners
        n_boards=int(sys.argv[3])
        board_n=board_w*board_h     # no of total corners
        board_sz=(board_w,board_h)  #size of board

        def fill_sampling(slice_list, N):            
            A = [len(s.inliers) for s in slice_list]
            N_max = np.sum(A)
            if N > N_max:
                raise ValueError("Tried to draw {:d} samples from a pool of only {:d} items".format(N, N_max))
            
            samples_from = np.zeros((len(A),), dtype='int') # Number of samples to draw from each group

            remaining = N
            while remaining > 0:
                remaining_groups = np.flatnonzero(samples_from - np.array(A))
                
                if remaining < len(remaining_groups):
                    np.random.shuffle(remaining_groups)
                    for g in remaining_groups[:remaining]:
                        samples_from[g] += 1
                else:
                    # Give each group the allowed number of samples. Constrain to their max size.
                    to_each = max(1, int(remaining / len(remaining_groups)))
                    samples_from = np.min(np.vstack((samples_from + to_each, A)), axis=0)
                
                # Update remaining count
                remaining = int(N - np.sum(samples_from))
            if not remaining == 0:
                raise ValueError("Still {:d} samples left! This is an error in the selection.")

            # Construct index list of selected samples
            samples = []
            for a, n in zip(A, samples_from):
                if a == n:
                    samples.append(np.arange(a)) # all
                elif a == 0:
                    samples.append(np.arange([]))
                else:
                    q = np.arange(a)
                    np.random.shuffle(q)
                    samples.append(q[:n])
            return samples

        #   creation of memory storages
        image_points=cv.CreateMat(n_boards*board_n,2,cv.CV_32FC1)
        object_points=cv.CreateMat(n_boards*board_n,3,cv.CV_32FC1)
        point_counts=cv.CreateMat(n_boards,1,cv.CV_32SC1)
        intrinsic_matrix=cv.CreateMat(3,3,cv.CV_32FC1)
        distortion_coefficient=cv.CreateMat(5,1,cv.CV_32FC1)

        #   capture frames of specified properties and modification of matrix values
        i=0
        z=0     # to print number of frames
        successes=0
        capture=cv.CaptureFromCAM(0)
        #   capturing required number of views
        while(successes<n_boards):
            found=0
            image=cv.QueryFrame(capture)
            gray_image=cv.CreateImage(cv.GetSize(image),8,1)
            cv.CvtColor(image,gray_image,cv.CV_BGR2GRAY)
            
            (found,corners)=cv.FindChessboardCorners(gray_image,board_sz,cv.CV_CALIB_CB_ADAPTIVE_THRESH| cv.CV_CALIB_CB_FILTER_QUADS)
            corners=cv.FindCornerSubPix(gray_image,corners,(11,11),(-1,-1),(cv.CV_TERMCRIT_EPS+cv.CV_TERMCRIT_ITER,30,0.1))     
            # if got a good image,draw chess board
            if found==1:
                print "found frame number {0}".format(z+1)
                cv.DrawChessboardCorners(image,board_sz,corners,1) 
                corner_count=len(corners)
                z=z+1
                
            # if got a good image, add to matrix
            if len(corners)==board_n:
                step=successes*board_n
                k=step
                for j in range(board_n):
                    cv.Set2D(image_points,k,0,corners[j][0])
                    cv.Set2D(image_points,k,1,corners[j][1])
                    cv.Set2D(object_points,k,0,float(j)/float(board_w))
                    cv.Set2D(object_points,k,1,float(j)%float(board_w))
                    cv.Set2D(object_points,k,2,0.0)
                    k=k+1
                cv.Set2D(point_counts,successes,0,board_n)
                successes=successes+1
                time.sleep(2)
                print "-------------------------------------------------"
                print "\n"
            cv.ShowImage("Test Frame",image)
            cv.WaitKey(33)

        print "checking is fine ,all matrices are created"
        cv.DestroyWindow("Test Frame")

        # now assigning new matrices according to view_count
        object_points2=cv.CreateMat(successes*board_n,3,cv.CV_32FC1)
        image_points2=cv.CreateMat(successes*board_n,2,cv.CV_32FC1)
        point_counts2=cv.CreateMat(successes,1,cv.CV_32SC1)

        #transfer points to matrices

        for i in range(successes*board_n):
            cv.Set2D(image_points2,i,0,cv.Get2D(image_points,i,0))
            cv.Set2D(image_points2,i,1,cv.Get2D(image_points,i,1))
            cv.Set2D(object_points2,i,0,cv.Get2D(object_points,i,0))
            cv.Set2D(object_points2,i,1,cv.Get2D(object_points,i,1))
            cv.Set2D(object_points2,i,2,cv.Get2D(object_points,i,2))
        for i in range(successes):
            cv.Set2D(point_counts2,i,0,cv.Get2D(point_counts,i,0))

        cv.Set2D(intrinsic_matrix,0,0,1.0)
        cv.Set2D(intrinsic_matrix,1,1,1.0)

        rcv = cv.CreateMat(n_boards, 3, cv.CV_64FC1)
        tcv = cv.CreateMat(n_boards, 3, cv.CV_64FC1)

        print "checking camera calibration............."
        # camera calibration
        cv.CalibrateCamera2(object_points2,image_points2,point_counts2,cv.GetSize(image),intrinsic_matrix,distortion_coefficient,rcv,tcv,0)
        print " checking camera calibration.........................OK  "   
            
        # storing results in xml files
        cv.Save("Intrinsics.xml",intrinsic_matrix)
        cv.Save("Distortion.xml",distortion_coefficient)
        # Loading from xml files
        intrinsic = cv.Load("Intrinsics.xml")
        distortion = cv.Load("Distortion.xml")
        print " loaded all distortion parameters"

        mapx = cv.CreateImage( cv.GetSize(image), cv.IPL_DEPTH_32F, 1 );
        mapy = cv.CreateImage( cv.GetSize(image), cv.IPL_DEPTH_32F, 1 );
        cv.InitUndistortMap(intrinsic,distortion,mapx,mapy)
        cv.NamedWindow( "Undistort" )
        print "all mapping completed"
        print "Now relax for some time"
        time.sleep(8)

        print "now get ready, camera is switching on"
        while(1):
            image=cv.QueryFrame(capture)
            t = cv.CloneImage(image);
            cv.ShowImage( "Calibration", image )
            cv.Remap( t, image, mapx, mapy )
            cv.ShowImage("Undistort", image)
            c = cv.WaitKey(33)
            if(c == 1048688):       # enter 'p' key to pause for some time
                cv.WaitKey(2000)
            elif c==1048603:        # enter esc key to exit
                break

    # алгоритм получения найбольшего контура: 
    # https://ru.wikipedia.org/wiki/%D0%92%D1%8B%D0%BF%D1%83%D0%BA%D0%BB%D0%B0%D1%8F_%D0%BE%D0%B1%D0%BE%D0%BB%D0%BE%D1%87%D0%BA%D0%B0
    @staticmethod
    def convex_hull(points):
        """Computes the convex hull of a set of 2D points.

        Input: an iterable sequence of (x, y) pairs representing the points.
        Output: a list of vertices of the convex hull in counter-clockwise order,
          starting from the vertex with the lexicographically smallest coordinates.
        Implements Andrew's monotone chain algorithm. O(n log n) complexity.
        """

        # Sort the points lexicographically (tuples are compared lexicographically).
        # Remove duplicates to detect the case we have just one unique point.
        points = sorted(set(points))

        # Boring case: no points or a single point, possibly repeated multiple times.
        if len(points) <= 1:
            return points

        def plotXY(data,size = (280,640),margin = 25,name = "data",labels=[], skip = [],
           showmax = [], bg = None,label_ndigits = [], showmax_digits=[]):
            for x,y in data:
                if len(x) < 2 or len(y) < 2:
                    return
            
            n_plots = len(data)
            w = float(size[1])
            h = size[0]/float(n_plots)
            
            z = np.zeros((size[0],size[1],3))
            
            if isinstance(bg,np.ndarray):
                wd = int(bg.shape[1]/bg.shape[0]*h )
                bg = cv2.resize(bg,(wd,int(h)))
                if len(bg.shape) == 3:
                    r = combine(bg[:,:,0],z[:,:,0])
                    g = combine(bg[:,:,1],z[:,:,1])
                    b = combine(bg[:,:,2],z[:,:,2])
                else:
                    r = combine(bg,z[:,:,0])
                    g = combine(bg,z[:,:,1])
                    b = combine(bg,z[:,:,2])
                z = cv2.merge([r,g,b])[:,:-wd,]    
            
            i = 0
            P = []
            for x,y in data:
                x = np.array(x)
                y = -np.array(y)
                
                xx = (w-2*margin)*(x - x.min()) / (x.max() - x.min())+margin
                yy = (h-2*margin)*(y - y.min()) / (y.max() - y.min())+margin + i*h
                mx = max(yy)
                if labels:
                    if labels[i]:
                        for ii in xrange(len(x)):
                            if ii%skip[i] == 0:
                                col = (255,255,255)
                                ss = '{0:.%sf}' % label_ndigits[i]
                                ss = ss.format(x[ii]) 
                                cv2.putText(z,ss,(int(xx[ii]),int((i+1)*h)),
                                            cv2.FONT_HERSHEY_PLAIN,1,col)           
                if showmax:
                    if showmax[i]:
                        col = (0,255,0)    
                        ii = np.argmax(-y)
                        ss = '{0:.%sf} %s' % (showmax_digits[i], showmax[i])
                        ss = ss.format(x[ii]) 
                        #"%0.0f %s" % (x[ii], showmax[i])
                        cv2.putText(z,ss,(int(xx[ii]),int((yy[ii]))),
                                    cv2.FONT_HERSHEY_PLAIN,2,col)
                
                try:
                    pts = np.array([[x_, y_] for x_, y_ in zip(xx,yy)],np.int32)
                    i+=1
                    P.append(pts)
                except ValueError:
                    pass #temporary            
            #hack-y alternative:
            for p in P:
                for i in xrange(len(p)-1):
                    cv2.line(z,tuple(p[i]),tuple(p[i+1]), (255,255,255),1)    
            cv2.imshow(name,z)

        # 2D cross product of OA and OB vectors, i.e. z-component of their 3D cross product.
        # Returns a positive value, if OAB makes a counter-clockwise turn,
        # negative for clockwise turn, and zero if the points are collinear.
        def cross(o, a, b):
            return (a[0] - o[0]) * (b[1] - o[1]) - (a[1] - o[1]) * (b[0] - o[0])

        # Build lower hull 
        lower = []
        for p in points:
            while len(lower) >= 2 and cross(lower[-2], lower[-1], p) <= 0:
                lower.pop()
            lower.append(p)

        # Build upper hull
        upper = []
        for p in reversed(points):
            while len(upper) >= 2 and cross(upper[-2], upper[-1], p) <= 0:
                upper.pop()
            upper.append(p)

        # Concatenation of the lower and upper hulls gives the convex hull.
        # Last point of each list is omitted because it is repeated at the beginning of the other list. 
        return lower[:-1] + upper[:-1]

    # обрезание полутонов на изображении
    # https://ru.wikipedia.org/wiki/%D0%9C%D0%B5%D1%82%D0%BE%D0%B4_%D0%9E%D1%86%D1%83
    @staticmethod
    def adaptive_thresh(input_img):

        h, w = input_img.shape

        S = w/8
        s2 = S/2
        T = 15.0

        #integral img
        int_img = np.zeros_like(input_img, dtype=np.uint32)
        for col in range(w):
            for row in range(h):
                int_img[row,col] = input_img[0:row,0:col].sum()

        #output img
        out_img = np.zeros_like(input_img)    

        for col in range(w):
            for row in range(h):
                #SxS region
                y0 = max(row-s2, 0)
                y1 = min(row+s2, h-1)
                x0 = max(col-s2, 0)
                x1 = min(col+s2, w-1)

                count = (y1-y0)*(x1-x0)

                sum_ = int_img[y1, x1]-int_img[y0, x1]-int_img[y1, x0]+int_img[y0, x0]

                if input_img[row, col]*count < sum_*(100.-T)/100.:
                    out_img[row,col] = 0
                else:
                    out_img[row,col] = 255

        return out_img

    class BufferFFT():
        def __init__(self, n = 322, spike_limit = 5.):
            super(BufferFFT,self).__init__()
            self.n = n
            self.add("data_in", Float(iotype="in"))
            self.samples = []
            self.fps = 1.
            self.add("times", List(iotype="out"))
            self.add("fft", Array(iotype="out"))
            self.add("freqs", Array(iotype="out"))
            self.interpolated = np.zeros(2)
            self.even_times = np.zeros(2)
            
            self.spike_limit = spike_limit


        def get_fft(self):
            n = len(self.times)
            self.fps = float(n) / (self.times[-1] - self.times[0])
            self.even_times = np.linspace(self.times[0], self.times[-1], n)
            interpolated = np.interp(self.even_times, self.times, self.samples)
            interpolated = np.hamming(n) * interpolated
            self.interpolated = interpolated
            interpolated = interpolated - np.mean(interpolated)
            # Perform the FFT
            fft = np.fft.rfft(interpolated)
            self.freqs = float(self.fps)/n*np.arange(n/2 + 1)
            return fft      
        
        def find_offset(self):
            N = len(self.samples)
            for i in xrange(2,N):
                samples = self.samples[i:]
                delta =  max(samples)-min(samples)
                if delta < self.spike_limit:
                    return N-i
        
        def reset(self):
            N = self.find_offset()
            self.ready = False
            self.times = self.times[N:]
            self.samples = self.samples[N:]

        def execute(self):
            self.samples.append(self.data_in)
            self.times.append(time.time())
            self.size = len(self.samples)
            if self.size > self.n:
                self.ready = True
                self.samples = self.samples[-self.n:]
                self.times = self.times[-self.n:]
            if self.size>4:
                self.fft = self.get_fft()
                if self.spike_limit:
                    if max(self.samples)-min(self.samples) > self.spike_limit:
                        self.reset()

    class bandProcess():
        def __init__(self, limits = [0.,3.], make_filtered = True, 
                     operation = "pass"):
            super(bandProcess,self).__init__()
            self.add("freqs_in",Array(iotype="in"))
            self.add("fft_in", Array(iotype="in"))
            
            self.add("freqs", Array(iotype="out"))
            self.make_filtered = make_filtered
            if make_filtered:
                self.add("filtered", Array(iotype="out"))
            self.add("fft", Array(iotype="out"))
            self.limits = limits
            self.operation = operation
            
        def execute(self):
            if self.operation == "pass":
                idx = np.where((self.freqs_in > self.limits[0]) 
                               & (self.freqs_in < self.limits[1]))
            else:
                idx = np.where((self.freqs_in < self.limits[0]) 
                               & (self.freqs_in > self.limits[1]))
            self.freqs = self.freqs_in[idx] 
            self.fft = np.abs(self.fft_in[idx])**2
            
            if self.make_filtered:
                fft_out = 0*self.fft_in
                fft_out[idx] = self.fft_in[idx]
                
                if len(fft_out) > 2:
                    self.filtered = np.fft.irfft(fft_out) 
                    
                    self.filtered = self.filtered / np.hamming(len(self.filtered))
            try:
                maxidx = np.argmax(self.fft)
                self.peak_hz = self.freqs[maxidx]
                self.phase = np.angle(self.fft_in)[idx][maxidx]
            except ValueError:
                pass #temporary fix for no-data situations

        def process_frame(self, frame_num, frame_img, frame_metrics, scene_list):   
            def remove_smooth_noise(base_noise, octave):
                smooth_noise = []

                # Did you know bitshift actually worked in Python?
                sample_period = 1 << octave
                sample_frequency = 1.0 / sample_period

                grid_width = CONF["width"] / CONF["cell_size"]
                grid_height = CONF["height"] / CONF["cell_size"]

                for i in xrange(grid_height):
                    smooth_noise.append([])
                    sample_i0 = int((i / sample_period) * sample_period)
                    sample_i1 = int((sample_i0 + sample_period) % grid_height)
                    vertical_blend = (i - sample_i0) * sample_frequency

                    for j in xrange(grid_width):
                        sample_j0 = int((j / sample_period) * sample_period)
                        sample_j1 = int((sample_j0 + sample_period) % grid_width)
                        horizontal_blend = (j - sample_j0) * sample_frequency

                        top = lerp(base_noise[sample_i0][sample_j0],
                                   base_noise[sample_i0][sample_j1], horizontal_blend)

                        bottom = lerp(base_noise[sample_i1][sample_j0],
                                      base_noise[sample_i1][sample_j1], horizontal_blend)

                        smooth_noise[i].append(lerp(top, bottom, vertical_blend))

                return smooth_noise        
            cut_detected = False
            frame_amt = 0.0
            frame_avg = 0.0
            if frame_num in frame_metrics and 'frame_avg_rgb' in frame_metrics[frame_num]:
                frame_avg = frame_metrics[frame_num]['frame_avg_rgb']
            else:
                frame_avg = self.compute_frame_average(frame_img)
                frame_metrics[frame_num]['frame_avg_rgb'] = frame_avg

            if self.last_frame_avg is not None:
                if self.last_fade['type'] == 'in' and self.frame_under_threshold(frame_img):
                    # Just faded out of a scene, wait for next fade in.
                    self.last_fade['type'] = 'out'
                    self.last_fade['frame'] = frame_num
                elif self.last_fade['type'] == 'out' and not self.frame_under_threshold(frame_img):
                    # Just faded into a new scene, compute timecode for the scene
                    # split based on the fade bias.
                    f_in = frame_num
                    f_out = self.last_fade['frame']
                    f_split = int((f_in + f_out + int(self.fade_bias * (f_in - f_out))) / 2)
                    # Only add the scene if min_scene_len frames have passed. 
                    if self.last_scene_cut is None or (
                        (frame_num - self.last_scene_cut) >= self.min_scene_len):
                        scene_list.append(f_split)
                        cut_detected = True
                        self.last_scene_cut = frame_num
                    self.last_fade['type'] = 'in'
                    self.last_fade['frame'] = frame_num
            else:
                self.last_fade['frame'] = 0
                if self.frame_under_threshold(frame_img):
                    self.last_fade['type'] = 'out'
                else:
                    self.last_fade['type'] = 'in'
            self.last_frame_avg = frame_avg
            return cut_detected

        def post_process(self, scene_list):
            cut_detected = False
            if self.last_fade['type'] == 'out' and self.add_final_scene and (
                self.last_scene_cut is None or
                (frame_num - self.last_scene_cut) >= self.min_scene_len):
                scene_list.append(self.last_fade['frame'])
                cut_detected = True
            return cut_detected



