# -*- coding: UTF8 -*-

import imutils
import cv2
from image_optimizer import ImageOptimizer
import numpy as np
import sys, time, copy, math

# Конфиги
debug = False
# Время настройки
TIME_WAIT = 50
# Время принятия числа (сколько ждать, прежде чем число будет считано в формулу)
TIME_NUMBER_APPLY = 20

# глобальные переменные
numberBuffer = []
currentNumber = ""
oldNumber = ""
currentFrame = 0
expr = ""

bgs = cv2.BackgroundSubtractorMOG2()
if cv2 != None:
	ImageOptimizer.convex_hull = cv2.convexHull
	ImageOptimizer.threshold = cv2.threshold

def main():	
	setup = False
	# получаем объект для чтения
	if len(sys.argv) > 1:
		cap = cv2.VideoCapture(sys.argv[1])
	else:
		cap = cv2.VideoCapture(0)	

	# процедура, которая выполняется в вечном цикле
	def do(callback=None, background_frame=None):
		global numberBuffer, currentNumber, oldNumber, currentFrame, expr	

		# чтение фрейма с камеры
		_, frame = cap.read()
		# если передана функция callback, выполняем ее тоже
		if callback is not None:
			callback(frame)
		# если мы считали уже бекграунд фрейм (т.е. настройка окончена), начинаем работу алгоритма
		if background_frame is not None:
			# процессинг данных и поиск экстремумов
			process(frame, background_frame)
			# рисуем зеленый квадрат
			cv2.rectangle(frame,(600,600),(100,100),(0,255,0),0)

			# если число считанное на -1 и не меняется на протяжении времени TIME_NUMBER_APPLY, то подсчитываем формулу
			if currentNumber == -1:
				pass
			elif oldNumber != currentNumber:
				oldNumber = currentNumber
				currentFrame = 0
			elif currentFrame != TIME_NUMBER_APPLY:
				currentFrame += 1
			else:				
				# обнуляем буферные значения
				currentFrame = 0
				oldNumber = -1

				# если число валидно, добавляем его в буфер чисел в формуле
				if currentNumber != 0 and currentNumber != '':
					numberBuffer.append(int(currentNumber))

				# если же считали число 0 - сумируем и выдаем результат в виде формулы с равенством: 5+5=10
				if currentNumber == 0:
					sum_number = sum(numberBuffer)					
					expr = " + ".join(str(num) for num in numberBuffer)	
					expr += " = " + str(sum_number)
					print(expr)
					numberBuffer = []
				# если число не 0, и числовой буфер не пустой, выводим формулу на экран: 5+5
				elif len(numberBuffer) > 0:
					expr = " + ".join(str(num) for num in numberBuffer)
					print(expr)
				# если это первое считанное число в формуле, то просто показываем его
				else:
					expr = str(currentNumber)

				if expr == ' = 0':
					expr = ''
				
		# вывод выражения в фрейм
		cv2.putText(frame, expr, (5,100), cv2.FONT_HERSHEY_SIMPLEX, 1, 2)
		# вывод фрейма на экран
		cv2.imshow("Gesture recognizer", frame)
		return frame
	
	# callback-функция, которая передается только во время настройки и выводит красный квадрат
	def callback_before(frame):
		cv2.putText(frame, "Wait for background to set up", (5,50), cv2.FONT_HERSHEY_SIMPLEX, 1, 2)
		cv2.rectangle(frame,(600,600),(100,100),(0,0,255),0)

	# ждем настройку
	background_frame = None
	i = 0	
	while (i < TIME_WAIT):
		# считываем бекграунд-фрейм
		background_frame = do(callback=callback_before)
		i += 1
		# если нажали ESC - выходим из проги
		k = cv2.waitKey(30) & 0xff		
		if k == 27:
			break
	
	# set exposure value for camera: http://stackoverflow.com/questions/11420748/setting-camera-parameters-in-opencv-python
	#	cap.set(21, 0)
	while True:
		# передаем бекграунд фрейм в процедуру для обработки и подсчета
		do(background_frame=background_frame)
		# если нажали ESC - выходим из проги
		k = cv2.waitKey(30) & 0xff		
		if k == 27:
			break


# главная процедура обработки фрейма и подсчета экстремумов
def process(frame, background_frame):
	global currentNumber, bgs

	# вырезаем кусок фрейма с которым будем работать
	image = frame[100:600, 100:600]

	# отнимаем от текущей картинки то, что было в бекграунд-фрейме, чтобы получить больше точность
	background = background_frame[100:600, 100:600]
	sub = cv2.subtract(background, image)

	# используем встроенный opencv фильтр для очистки заднего фона
	fgmask = bgs.apply(sub)
	gray = cv2.bitwise_and(sub,sub,mask=fgmask)
	
	# конвертируем в серый и выравниваем контраст
	gray = cv2.cvtColor(gray, cv2.COLOR_BGR2GRAY)
	cv2.equalizeHist(gray)
	#gray = cv2.GaussianBlur(gray, (5, 5), 0)

	if debug:
		cv2.imshow("Grayed-out", gray)

	# используем операции opencv threshold, erode, dilate, чтобы убрать мелкие источники шума
	thresh = ImageOptimizer.threshold(gray, 45, 255, cv2.THRESH_BINARY)[1]
	thresh = cv2.erode(thresh, None, iterations=1)
	thresh = cv2.dilate(thresh, None, iterations=10)

	# выполняем поиск найбольшего контура
	cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
		cv2.CHAIN_APPROX_NONE)
	cnts = cnts[0] if imutils.is_cv2() else cnts[1]
	if len(cnts) == 0:
		currentNumber = -1
		return
	cnt = max(cnts, key=cv2.contourArea)

	# находим точки пересечения пальцев
	#
	x,y,w,h = cv2.boundingRect(cnt)
	# рисуем квадрат вокруг руки
	cv2.rectangle(image,(x,y),(x+w,y+h),(0,0,255),0)
	# рисуем полигон вокруг руки
	hull = ImageOptimizer.convex_hull(cnt)
	drawing = np.zeros(image.shape,np.uint8)
	cv2.drawContours(drawing,[cnt],0,(0,255,0),0)
	cv2.drawContours(drawing,[hull],0,(0,0,255),0)
	hull = ImageOptimizer.convex_hull(cnt,returnPoints = False)
	# находим дефекты в контуре и считаем их	
	defects = cv2.convexityDefects(cnt,hull)
	count_defects = 0
	# находим средние точки между пальцами	
	if defects is not None:
		cv2.drawContours(thresh, cnts, -1, (0,255,0), 3)
		for i in range(defects.shape[0]):
			s,e,f,d = defects[i,0]
			start = tuple(cnt[s][0])
			end = tuple(cnt[e][0])
			far = tuple(cnt[f][0])
			a = math.sqrt((end[0] - start[0])**2 + (end[1] - start[1])**2)
			b = math.sqrt((far[0] - start[0])**2 + (far[1] - start[1])**2)
			c = math.sqrt((end[0] - far[0])**2 + (end[1] - far[1])**2)
			angle = math.acos((b**2 + c**2 - a**2)/(2*b*c)) * 57
			# если угол между пальцами <= 90, то мы нашли дефект
			if angle <= 90:
				count_defects += 1
				# рисуем точку
				cv2.circle(image,far, 6,[0,0,255],-1)				
			cv2.line(image,start,end,[0,255,0],2)

	# находим экстремумы функции (макс верхнюю точку, макс правую, нижнюю, левую)
	c = cnt
	extLeft = tuple(c[c[:, :, 0].argmin()][0])
	extRight = tuple(c[c[:, :, 0].argmax()][0])
	extTop = tuple(c[c[:, :, 1].argmin()][0])
	extBot = tuple(c[c[:, :, 1].argmax()][0])

	# рисуем рисуем контуры руки и 3 экстремума - верх-лево-право
	cv2.drawContours(image, [c], -1, (0, 255, 255), 3)
	cv2.circle(image, extLeft, 6, (0, 0, 255), -1)
	cv2.circle(image, extRight, 6, (0, 255, 0), -1)
	cv2.circle(image, extTop, 6, (255, 0, 0), -1)

	# считаем дефекты, от кол-ва дефектов зависит кол-во пальцев
	text = ""
	if count_defects == 0:
		text = "1"
	elif count_defects == 1:
		text = "2"
	elif count_defects == 2:
		text = "3"
	elif count_defects == 3:
		text = "4"
	elif count_defects == 4:
		text = "5"
	
	# если мы сжали руку в кулак (то наши координаты будут примерно в этом уравнении), то это - 0
	if (((extRight[0] - extTop[0] < 150 and extRight[1] - extTop[1] > 50  \
		and extRight[1] - extTop[1] < 100) or extTop[0] - extRight[0] < 150 and extTop[1] - extRight[1] > 50  \
		and extTop[1] - extRight[1] < 100)) and count_defects >= 0 and count_defects < 2:
		text = "0"
	
	# выводим число на экран
	currentNumber = int(text if text != "" else "-1")
	cv2.putText(frame, "Number: " + text, (5,50), cv2.FONT_HERSHEY_SIMPLEX, 1, 2)
	if debug:
		cv2.putText(frame, "Number: %s (%s, %s)" % (text, str(extRight[0]), str(extTop[0])), (5,50), cv2.FONT_HERSHEY_SIMPLEX, 1, 2)	


# запуск программы
if __name__ == '__main__':
	main()




